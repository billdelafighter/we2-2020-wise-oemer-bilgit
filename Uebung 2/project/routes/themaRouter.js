var express = require('express');
var router = express.Router();

var mongoUtils = require('../bin/mongoUtils');



/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// im Browser so aufrunfen zB. http://localhost:3000/oberthema
router.get('/oberthema/', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').find().toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2
router.get('/oberthema/:id/', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').find({ _id: parseInt(req.params.id) }).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema
router.get('/oberthema/:id/unterthema/', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$group': {
        '_id': '$unterthema'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema/1
router.get('/oberthema/:id/unterthema/:u_id', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$match': {
        'unterthema.unterthema_id': parseInt(req.params.u_id)
      }
    }, {
      '$group': {
        '_id': '$unterthema'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema/1/forumbeitrag
router.get('/oberthema/:id/unterthema/:u_id/forumbeitrag', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$match': {
        'unterthema.unterthema_id': parseInt(req.params.u_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag'
      }
    }, {
      '$group': {
        '_id': '$unterthema.forumbeitrag'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema/1/forumbeitrag/2
router.get('/oberthema/:id/unterthema/:u_id/forumbeitrag/:f_id', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$match': {
        'unterthema.unterthema_id': parseInt(req.params.u_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag'
      }
    }, {
      '$match': {
        'unterthema.forumbeitrag.forumbeitrag_id': parseInt(req.params.f_id)
      }
    }, {
      '$group': {
        '_id': '$unterthema.forumbeitrag'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema/1/forumbeitrag/1/post
router.get('/oberthema/:id/unterthema/:u_id/forumbeitrag/:f_id/post', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$match': {
        'unterthema.unterthema_id': parseInt(req.params.u_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag'
      }
    }, {
      '$match': {
        'unterthema.forumbeitrag.forumbeitrag_id': parseInt(req.params.f_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag.posts'
      }
    },{
      '$group': {
        '_id': '$unterthema.forumbeitrag.posts'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/oberthema/2/unterthema/1/forumbeitrag/1/post/2
router.get('/oberthema/:id/unterthema/:u_id/forumbeitrag/:f_id/post/:p_id', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('forum').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema'
      }
    }, {
      '$match': {
        'unterthema.unterthema_id': parseInt(req.params.u_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag'
      }
    }, {
      '$match': {
        'unterthema.forumbeitrag.forumbeitrag_id': parseInt(req.params.f_id)
      }
    }, {
      '$unwind': {
        'path': '$unterthema.forumbeitrag.posts'
      }
    }, {
      '$match': {
        'unterthema.forumbeitrag.posts.post_id': parseInt(req.params.p_id)
      }
    },{
      '$group': {
        '_id': '$unterthema.forumbeitrag.posts'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

module.exports = router;