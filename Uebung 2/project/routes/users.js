var express = require('express');
var router = express.Router();

var mongoUtils = require('../bin/mongoUtils');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

// User collection
// im Browser so aufrunfen zB. http://localhost:3000/user
router.get('/user', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('user').find().toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/user/2
router.get('/user/:id/', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('user').find({ _id: parseInt(req.params.id) }).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// im Browser so aufrunfen zB. http://localhost:3000/user/2/scores
router.get('/user/:id/scores', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('user').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$scores'
      }
    }, {
      '$group': {
        '_id': '$scores'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

// Finde vergebene Bewertung anhand des usernames
// im Browser so aufrunfen zB. http://localhost:3000/user/2/scores/Kristina
router.get('/user/:id/scores/:s_id', function (req, res) {
  var db = mongoUtils.getDb();
  db.collection('user').aggregate([
    {
      '$match': {
        '_id': parseInt(req.params.id)
      }
    }, {
      '$unwind': {
        'path': '$scores'
      }
    }, {
      '$match': {
        'scores.user_id': req.params.s_id
      }
    }, {
      '$group': {
        '_id': '$scores'
      }
    }
  ]).toArray()
    .then(results => {
      console.log(results);
      res.json(results);
    })
    .catch(error => console.error(error))
})

module.exports = router;
