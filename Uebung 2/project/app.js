var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var themaRouter = require('./routes/themaRouter');

var app = express();

// var MongoClient = require('mongodb').MongoClient;
// var url ='mongodb://localhost/';
// const client = new MongoClient(url, {useUnifiedTopology: true});

// client.connect().then((client) =>{
//   var db = client.db('app');
//   db.collection('startup_log').find().toArray(function(err, result){
//     if(err) {
//       throw err;
//     }
//     console.log(result);
//     var cursor = db.collection('forum').find();
//     cursor.each(function(err, doc){
//       console.log(doc);
//     })
//   })
// })

var mongoUtil = require('./bin/mongoUtils');



mongoUtil.connectToServer(function (err, client) {
  if (err) console.log(err);
  // start the rest of your app here

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  app.use('/', indexRouter);
  app.use('/', usersRouter);
  app.use('/', themaRouter);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    next(createError(404));
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });
});




module.exports = app;
